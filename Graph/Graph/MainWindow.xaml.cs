﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using QuickGraph;
using QuickGraph.Graphviz;
using QuickGraph.Graphviz.Dot;
using examples.Support;
using System.Diagnostics;

namespace Graph
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private CurGraph graph;
        AdjacencyGraph<string, TaggedEdge<string, string>> vizDirectedGraph;
        AdjacencyGraph<string, /*TaggedUndirectedEdge*/TaggedEdge<string, string>> vizUndirectedGraph;


        public MainWindow()
        {
            InitializeComponent();
            InitializeMyControls();
            graph = new CurGraph();
        }

        private void InitializeMyControls()
        {
            //cbOperations.SelectedIndex = 0;
            lbMark.Visibility = Visibility.Hidden;
            tbMark.Visibility = Visibility.Hidden;
            cbVertexes.Visibility = Visibility.Hidden;

            lb1.Visibility = Visibility.Hidden;
            lb2.Visibility = Visibility.Hidden;

            cb1.Visibility = Visibility.Hidden;
            cb2.Visibility = Visibility.Hidden;

        }

        private void OpenAdjListItem_Click(object sender, RoutedEventArgs e)
        {
            open(FileType.adjacencyList);
        }

        private void open(FileType ft)
        {
            OpenFileDialog openFileDialog = openGraph();
            graph.fillGraphStructure(openFileDialog, ft);
            initializeGraph();
            tbResult.Text = "";
        }

        private void OpenAdjMatrixItem_Click(object sender, RoutedEventArgs e)
        {
            open(FileType.adjacencyMatrix);
        }


        private void initializeGraph()
        {
            graph.toUndirected();
            if (graph.getOriented())
                visualizeDirectedGraph();
            else
                visualizeUndirectedGraph();
            showInfo();
            initializeComboBox(cbVertexes);
            initializeComboBox(cb1);
            initializeComboBox(cb2);
        }


        private void showInfo()
        {
            int connectComp = graph.getConnectivityComponents();
            bool isOriented = graph.getOriented();
            AbsoluteCenter abs = graph.GetAbsoluteCenter();
            tbInfo.Text = "Граф " + (isOriented ? "ориентированный" : "неориентированный") + "\n" +
                "Количество вершин: " + graph.vertexLabels.Count.ToString() + "\n" +
                            "Количество ребер: " + (isOriented ? vizDirectedGraph.EdgeCount.ToString() : (vizUndirectedGraph.EdgeCount / 2).ToString()) + "\n" +
                            "Макс. степень вершин: " + graph.getMaxVertexDegree() + "\n" +
                            "Число слабо связных компонентов : " + connectComp.ToString() + "\n" +
                            "Диаметр: " + ((connectComp == 1) ? (graph.findDiameter().ToString()) : "граф не является связным") + "\n" +
                            "Радиус: " + ((connectComp == 1) ? (graph.findRadius().ToString()) : "граф не является связным") + "\n" +
                            "Цикломатическое число: " + ((connectComp == 1) ? getCyclomaticNumber(isOriented, connectComp) : "граф должен быть связным") + "\n" +
                            "Обход: " + ((!graph.getOriented()) ? graph.obhod : graph.getOrientedObhod()) + "\n" +
                            "Дерево: " + checkTree() + "\n" + "\n" +

                            "АБСОЛЮТНЫЙ ЦЕНТР (для неориентированных графов)" + "\n" +
                            "Ребро/вершина: " + abs.center.ToString() + "\n" +
                            "Радиус: " + abs.result.ToString() + "\n" +
                            "Точка: " + abs.point.ToString();
            //Пусть m - число ребер, n - число вершин, p - число компонент связности. Цикломатическое число K = m - n + p
            //Цикломатическое число графа указывает то наименьшее число рёбер, 
            //которое нужно удалить из данного графа, чтобы получить дерево (для связного графа) или лес 
            //(для несвязного графа), т.е. добиться отсутствия у графа циклов

            //цикломатическое число у ориент графа = цикл числу неориент графа (убрать ориентацию дуг)
            //Цикломатическое число несвязного графа равно сумме цикломатических чисел связных компонент.
        }

        private string getCyclomaticNumber(bool isOriented, int connectComp)
        {
            if (isOriented)
                return ((connectComp != -1) ? (vizDirectedGraph.EdgeCount - vizDirectedGraph.VertexCount + connectComp).ToString() : "");
            else
                return ((connectComp != -1) ? ((vizUndirectedGraph.EdgeCount/2) - vizUndirectedGraph.VertexCount + connectComp).ToString() : "");
        }

        private OpenFileDialog openGraph()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                txtInput.Text = File.ReadAllText(openFileDialog.FileName);
            return openFileDialog;
        }


        private string checkTree()
        {
            if (!graph.checkTree())
                return "Нет";
            return "Да";
        }

        private void visualizeDirectedGraph()
        {
            vizDirectedGraph = new AdjacencyGraph<string, TaggedEdge<string, string>>();

            foreach (var vertexList in graph.edgesList)
            {
                foreach (CurGraph.Edge edge in vertexList.Value)
                {
                    if (graph.vertexLabels.ContainsKey(edge.v_end))
                        vizDirectedGraph.AddVerticesAndEdge(new TaggedEdge<string, string>(graph.vertexLabels[vertexList.Key], graph.vertexLabels[edge.v_end], edge.weight.ToString()));
                }
                if (vertexList.Value.Count == 0)
                    vizDirectedGraph.AddVertex(graph.vertexLabels[vertexList.Key]);

            }

            var graphViz = new GraphvizAlgorithm<string, TaggedEdge<string, string>>(vizDirectedGraph, @".\", QuickGraph.Graphviz.Dot.GraphvizImageType.Png);
            graphViz.FormatVertex += FormatVertex;
            graphViz.FormatEdge += FormatEdge;
            graphViz.Generate(new FileDotEngine(), "graphPNG");
            Console.ReadLine();
        }


        private void visualizeUndirectedGraph()
        {
            vizUndirectedGraph = new AdjacencyGraph<string, TaggedEdge<string, string>>();

            foreach (var vertexList in graph.edgesList)
            {
                foreach (CurGraph.Edge edge in vertexList.Value)
                {
                    if (graph.vertexLabels.ContainsKey(edge.v_end))
                        vizUndirectedGraph.AddVerticesAndEdge(new TaggedEdge<string, string>(graph.vertexLabels[vertexList.Key], graph.vertexLabels[edge.v_end], edge.weight.ToString()));
                }
                if (vertexList.Value.Count == 0)
                    vizUndirectedGraph.AddVertex(graph.vertexLabels[vertexList.Key]);

            }

            var graphViz = new GraphvizAlgorithm<string, TaggedEdge<string, string>>(vizUndirectedGraph, @".\", QuickGraph.Graphviz.Dot.GraphvizImageType.Png);
            graphViz.FormatVertex += FormatVertex;
            graphViz.FormatEdge += FormatEdge;
            graphViz.Generate(new FileDotEngine(), "graphPNG");
            Console.ReadLine();
        }


        private void FormatVertex(object sender, FormatVertexEventArgs<string> e)
        {
            e.VertexFormatter.Label = e.Vertex;
            e.VertexFormatter.Shape = QuickGraph.Graphviz.Dot.GraphvizVertexShape.Circle;
            e.VertexFormatter.StrokeColor = GraphvizColor.Black;
            e.VertexFormatter.Font = new GraphvizFont(System.Drawing.FontFamily.GenericSansSerif.ToString(), 12);
        }


        private void FormatEdge(object sender, FormatEdgeEventArgs<string, TaggedEdge<string, string>> e)
        {
            e.EdgeFormatter.Font = new QuickGraph.Graphviz.Dot.GraphvizFont(System.Drawing.FontFamily.GenericSansSerif.ToString(), 12);
            e.EdgeFormatter.FontGraphvizColor = GraphvizColor.Black;
            e.EdgeFormatter.StrokeGraphvizColor = GraphvizColor.Black;
            e.EdgeFormatter.Weight = Convert.ToDouble(e.Edge.Tag);
            e.EdgeFormatter.Label.Value = e.Edge.Tag;
        }
        private void FormatEdge(object sender, FormatEdgeEventArgs<string, TaggedUndirectedEdge<string, string>> e)
        {
            e.EdgeFormatter.Font = new QuickGraph.Graphviz.Dot.GraphvizFont(System.Drawing.FontFamily.GenericSansSerif.ToString(), 12);
            e.EdgeFormatter.FontGraphvizColor = GraphvizColor.Black;
            e.EdgeFormatter.StrokeGraphvizColor = GraphvizColor.Black;
            e.EdgeFormatter.Weight = Convert.ToDouble(e.Edge.Tag);
            e.EdgeFormatter.Label.Value = e.Edge.Tag;
        }

        private void initializeComboBox(System.Windows.Controls.ComboBox combobox)
        {
            combobox.Items.Clear();
            foreach (var vertex in graph.vertexLabels)
            {
                combobox.Items.Add(vertex.Value);
            }
            combobox.SelectedIndex = 0;
        }

        private void btnDo_Click(object sender, RoutedEventArgs e)
        {
            cbOperations.SelectedItem.ToString();
            int selectedIndex = cbOperations.SelectedIndex;

            string ver1, ver2;
            switch (selectedIndex)
            {
                case 0: //добавить вершину
                    graph.cleanParametres();

                    String mark = tbMark.Text;
                    if (mark != null || mark != "")
                        graph.addVertex(mark);

                    //tbMark.Text = "";
                    initializeGraph();
                    tbResult.Text = "Вершина добавлена";
                    showInfo();
                    break;

                case 1: //удалить вершину

                    graph.cleanParametres();
                    string vertexVal = cbVertexes.SelectedItem.ToString();
                    if (vertexVal != null)
                    {
                        graph.deleteVertex(vertexVal);
                        initializeGraph();
                    }
                    tbResult.Text = "Вершина удалена";
                    showInfo();

                    break;
                case 2: //добавить ребро
                    graph.cleanParametres();

                    ver1 = cb1.SelectedValue.ToString();
                    ver2 = cb2.SelectedValue.ToString();
                    int weight = Convert.ToInt32(tbMark.Text);

                    graph.addEdge(ver1, ver2, weight);
                    initializeGraph();
                    tbResult.Text = "Ребро добавлено";
                    showInfo();
                    break;
                case 3: //удалить ребро
                    graph.cleanParametres();

                    ver1 = cb1.SelectedValue.ToString();
                    ver2 = cb2.SelectedValue.ToString();
                    graph.deleteEdge(ver1, ver2);
                    initializeGraph();
                    tbResult.Text = "Ребро удалено";
                    showInfo();
                    break;
                case 4: //найти ребро и его вес
                    ver1 = cb1.SelectedValue.ToString();
                    ver2 = cb2.SelectedValue.ToString();
                    int res = graph.findEdge(ver1, ver2);
                    if (res != -1)
                        tbResult.Text = "Ребро найдено, вес = " + res.ToString();
                    else
                        tbResult.Text = "Ребро не найдено";

                    break;
                case 5: //найти смежные вершины
                    string result = "";
                    List<CurGraph.Edge> adjacentVer = new List<CurGraph.Edge>();
                    ver1 = cbVertexes.SelectedValue.ToString();
                    int k = graph.getKeyByValue(ver1);
                    if (k != -1)
                        adjacentVer = graph.undirectedEdgesList[k];
                    foreach (CurGraph.Edge ed in adjacentVer)
                    {
                        result += graph.vertexLabels[ed.v_end].ToString() + " ";
                    }
                    tbResult.Text = result /*+ graph.peer(k)*/;

                    break;
                case 6: //найти вершины по метке

                    ver1 = tbMark.Text;
                    int key = graph.getKeyByValue(ver1);
                    if (key != -1)
                        tbResult.Text = "Вершина существует";
                    else
                        tbResult.Text = "Такой вершины нет в графе";
                    break;

                case 7:
                    tbResult.Text = graph.FindMaxMatch_EdmondsAlg();
                    break;
                default:
                    break;
            }
        }

        private void hideEdgeEditingControls(Visibility visibility)
        {
            lb1.Visibility = visibility;
            lb2.Visibility = visibility;
            cb1.Visibility = visibility;
            cb2.Visibility = visibility;
        }

        private void btnDraw_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("mspaint.exe", "graphPNG.jpg");
        }

        private void cbOperations_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            setControlsValue();
        }

        private void setControlsValue()
        {
            cbOperations.SelectedItem.ToString();
            int selectedIndex = cbOperations.SelectedIndex;


            switch (selectedIndex)
            {
                case 0: //добавить вершину
                    hideEdgeEditingControls(Visibility.Hidden);

                    tbMark.Visibility = Visibility.Visible;
                    lbMark.Visibility = Visibility.Visible;
                    lbMark.Content = "Метка";

                    cbVertexes.Visibility = Visibility.Hidden;
                    break;
                case 1://удалить вершину
                    showVertexControls();
                    break;
                case 2: //добавить ребро
                    hideEdgeEditingControls(Visibility.Visible);
                    lbMark.Content = "Вес";
                    lbMark.Visibility = Visibility.Visible;
                    tbMark.Visibility = Visibility.Visible;
                    break;
                case 3://удалить ребро
                    showEdgesControls();
                    break;
                case 4://найти ребро и вес
                    showEdgesControls();
                    break;
                case 5://найти смежные вершины
                    showVertexControls();
                    break;
                case 6://найти вершину по метке
                    hideEdgeEditingControls(Visibility.Hidden);

                    tbMark.Visibility = Visibility.Visible;
                    lbMark.Visibility = Visibility.Visible;
                    lbMark.Content = "Метка";
                    cbVertexes.Visibility = Visibility.Hidden;
                    break;
                case 7:
                    hideEdgeEditingControls(Visibility.Hidden);

                    tbMark.Visibility = Visibility.Hidden;
                    lbMark.Visibility = Visibility.Hidden;
                    cbVertexes.Visibility = Visibility.Hidden;
                    break;
                default:
                    break;
            }
        }

        private void showEdgesControls()
        {
            hideEdgeEditingControls(Visibility.Visible);
            lbMark.Visibility = Visibility.Hidden;
            tbMark.Visibility = Visibility.Hidden;
            cbVertexes.Visibility = Visibility.Hidden;
        }

        private void showVertexControls()
        {
            hideEdgeEditingControls(Visibility.Hidden);
            tbMark.Visibility = Visibility.Hidden;

            cbVertexes.Visibility = Visibility.Visible;
            lbMark.Content = "Метка";
            lbMark.Visibility = Visibility.Visible;
        }

        private void cbVertexes_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {

        }

        private void OpenBergDescriptionItem_Click(object sender, RoutedEventArgs e)
        {
            open(FileType.BergDescription);
        }

        private void OpenEdgeListItem_Click(object sender, RoutedEventArgs e)
        {
            open(FileType.edgeList);
        }

        private void OpenEdgeIncidenceMatrix_Click(object sender, RoutedEventArgs e)
        {
            open(FileType.incidenceMatrix);
        }

    }
}
    


