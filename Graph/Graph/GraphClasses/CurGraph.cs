﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Graph;
using static Graph.CurGraph;

namespace Graph
{
    public struct AbsoluteCenter
    {
        public double result;      //радиус
        public double point;       //точка на ребре
        public string center;      //ребро или вершина
    }

    enum FileType
    {
        adjacencyList,
        adjacencyMatrix,
        incidenceMatrix,
        edgeList,
        BergDescription
    }

    class CurGraph
    {
        public Dictionary<int, List<Edge>> edgesList;
        public Dictionary<int, List<Edge>> undirectedEdgesList;
        public Dictionary<int, string> vertexLabels;
        private Dictionary<int, int> color;
        private List<int> maxweight;

        private int[,] distanceMatr;
        private int nConnectivityComponents;
        private bool isOriented = false;
        private bool isTree = true;
        private int startVerCount = -1; //начальное количество вершин (надо запомнить для добавления новых вершин)
        public string obhod = "";

        private AbsoluteCenter absoluteCenter;


        public CurGraph()
        {
            vertexLabels = new Dictionary<int, string>(20);
            edgesList = new Dictionary<int, List<Edge>>();
            nConnectivityComponents = -1;
        }

        public AbsoluteCenter GetAbsoluteCenter()
        {
            return absoluteCenter;
        }

        public bool getOriented()
        {
            return isOriented;
        }

        /// <summary>
        /// При удалении вершин и ребер надо почистить некоторые выводимые параметры.
        /// </summary>
        public void cleanParametres()
        {
            nConnectivityComponents = -1;
            isTree = true;
            obhod = "";
        }

        public void toUndirected()
        {
            undirectedEdgesList = copyDictData(edgesList);

            foreach (var vertexList in this.undirectedEdgesList)
            {
                foreach (var ed in vertexList.Value)
                {
                    int key = vertexList.Key;
                    int ver = ed.v_end;

                    Edge e = new Edge();
                    e.v_end = key;
                    e.weight = ed.weight;

                    if (!undirectedEdgesList[ver].Contains(e, new MyEdgeComparer()))
                    {
                        undirectedEdgesList[ver].Add(e);
                    }
                }
            }
        }

        private Dictionary<int, List<Edge>> copyDictData(Dictionary<int, List<Edge>> dictForCopy)
        {
            Dictionary<int, List<Edge>> undirectedEdgesList = new Dictionary<int, List<Edge>>();

            foreach (var vertexList in dictForCopy)
            {
                List<Edge> list = new List<Edge>();
                foreach (var ed in vertexList.Value)
                {
                    list.Add(ed);
                }

                undirectedEdgesList.Add(vertexList.Key, list);
            }
            return undirectedEdgesList;
        }


        /// <summary>
        /// Компонента связности – набор вершин графа, между любой парой которых существует путь.
        /// Я ищу только компоненты слабой связности для неориентированного графа
        /// </summary>
        /// <returns></returns>
        public int getConnectivityComponents()
        {
            //if (!isOriented)
            findConnectivityComponents();
            return nConnectivityComponents;
        }

     

        public struct Edge
        {
            public int v_end;
            public int weight;
        }

        public string getMaxVertexDegree()
        {
            int degree = -1;
            foreach (var vertexList in this.edgesList)
            {
                if (vertexList.Value.Count > degree)
                    degree = vertexList.Value.Count;
            }
            return degree.ToString();
        }

        /// <summary>
        /// Диаметром связного графа называется максимально возможное расстояние между двумя его вершинами
        /// </summary>
        /// <returns></returns>
        public int findDiameter()
        {
            return maxweight.Max();
        }

        /// <summary>
        /// Центром графа называется такая вершина, что максимальное расстояние между ней 
        /// и любой другой вершиной является наименьшим из всех возможных; это расстояние 
        /// называется радиусом графа.
        /// </summary>
        /// <returns></returns>
        public int findRadius()
        {
            if (maxweight.Min() != -1)
                return maxweight.Min();
            else
            {
                maxweight.Sort();
                return maxweight[1];
            }
        }

        /// <summary>
        /// Ищем максимальное значение в каждой строчке матрицы расстояний.
        /// </summary>
        private void findMaxWeight()
        {
            maxweight = new List<int>();
            int n = this.edgesList.Count;

            for (int i = 0; i < n; i++)
            {
                int max = -1;
                for (int j = 0; j < n; j++)
                {
                    if (distanceMatr[i, j] != 1000 && distanceMatr[i, j] != 0)
                        if (distanceMatr[i, j] > max)
                            max = distanceMatr[i, j];
                }
                maxweight.Add(max);
            }
        }

        /// <summary>
        /// Создаем матрицу смежности, по которой в дальнейшем
        /// будем делать матрицу расстояний (по алгоритму Флойда)
        /// </summary>
        public void createAdjacencyMatrix(Dictionary<int, List<Edge>> edList)
        {
            Dictionary<int, int> myIndexMap = new Dictionary<int, int>();
            int nCount = 0;
            foreach (KeyValuePair<int, string> entry in vertexLabels)
            {
                myIndexMap.Add(entry.Key, nCount);
                nCount++;
            }

            distanceMatr = getDistanceArray(edList, myIndexMap);
            findMaxWeight();
            findAbsoluteCenter(myIndexMap);
        }

        private int [,] getDistanceArray(Dictionary<int, List<Edge>> edList, Dictionary<int, int> myIndexMap)
        {
            int n = edList.Count;
            int[,] matrix = new int[n, n];

            matrix = createAdjancyMatrix(edList, myIndexMap);

            //матрица смежности создана, идем далее
            Floyd(n, matrix);
            nullDiagonal(matrix);
            return matrix;
        }

        private static int[,] createAdjancyMatrix(Dictionary<int, List<Edge>> edList, Dictionary<int, int> myIndexMap)
        {
            int n = edList.Count;
            int[,] matrix = new int[n, n];
            initializeMatrix(n, matrix);

            foreach (var vertexList in edList)
            {
                int index = myIndexMap[vertexList.Key];
                for (int i = 0; i < vertexList.Value.Count; i++)
                {
                    int index2 = myIndexMap[vertexList.Value[i].v_end];

                    if (index == index2)
                        matrix[index, index2] = 0;
                    else
                        matrix[index, index2] = vertexList.Value[i].weight;
                }
            }
            return matrix;
        }

        private void nullDiagonal(int[,] distanceMatr)
        {
            for (int i = 0; i < vertexLabels.Count; i++)
                for (int j = 0; j < vertexLabels.Count; j++)
                    if (i == j)
                        distanceMatr[i, j] = 0;
        }

        void dfs(int vertex, Dictionary<int, List<Edge>> curDict)
        {
            
            color[vertex] = 1; //вход в вершину
            obhod += vertexLabels[vertex].ToString() + "  ";

            foreach (Edge edge in curDict[vertex])
            {
                if (color[edge.v_end] == 0)
                    dfs(edge.v_end,curDict);
                if (color[edge.v_end] == 1)
                {
                    isTree = false;
                    //return;
                }
            }
            color[vertex] = 2;
            obhod += vertexLabels[vertex].ToString() + "  ";

        }

        public string  getOrientedObhod()
        {
            isTree = true;
            obhod = "";
            color = new Dictionary<int, int>();
            initializeArray(color);

            for (int i = 0; i < vertexLabels.Count; i++)
            {
                int vertex = vertexLabels.ElementAt(i).Key;
                if (color[vertex] == 0)
                {
                    dfs(vertex, edgesList);
                }
            }
            return obhod;
        }

        private int findIndexInDictionary(int v_end)
        {
            int index = -1;
            for (int i = 0; i < vertexLabels.Count; i++)
            {
                if (vertexLabels.ElementAt(i).Key == v_end)
                    index = i;
            }
            return index;
        }

        /// <summary>
        /// Ищу слабую компоненту связности, поэтому обхожу неориентированный граф
        /// </summary>
        /// <returns></returns>
        private int findConnectivityComponents()
        {
            color = new Dictionary<int, int>();
            initializeArray(color);

            int cCount = 0;
            for (int i = 0; i < vertexLabels.Count; i++)
            {
                int vertex = vertexLabels.ElementAt(i).Key;
                if (color[vertex] == 0)
                {
                    dfs(vertex, undirectedEdgesList);
                    cCount++;
                }
            }
            nConnectivityComponents = cCount;
            return cCount;
        }

        public int findEdge(string ver1, string ver2)
        {
            int key = -1, key1 = -1;
            key = getKeyByValue(ver1);
            key1 = getKeyByValue(ver2);
            if (key == -1 || key1 == -1)
                return -1;
            foreach (Edge edge in edgesList[key])
            {
                if (edge.v_end == key1)
                    return edge.weight;
            }

            return -1;
          
        }

        public void addEdge(string ver1, string ver2, int weight)
        {
            isTree = true;
            int key = getKeyByValue(ver2);

            Edge ed = new Edge();

            ed.v_end = key;
            ed.weight = weight;

            key = getKeyByValue(ver1);

            edgesList[key].Add(ed);
            update();

        }

        public int getKeyByValue(string value)
        {
            int key = -1;
            if (vertexLabels.ContainsValue(value))
                key = vertexLabels.FirstOrDefault(x => x.Value == value).Key;

            return key;
        }

        public void addVertex(string vertexVal)
        {
            isTree = true;
            startVerCount++;
            vertexLabels.Add(startVerCount, vertexVal);
            List<Edge> list = new List<Edge>();
            edgesList.Add(startVerCount, list);
            update();

        }

        public void deleteVertex(string vertexVal)
        {
            isTree = true;
            int key = getKeyByValue(vertexVal);
            if (key != -1)
            {
                vertexLabels.Remove(key);
                //удаляем связи в edgesList
                deleteEdges(key);
            }
            update();
        }
        private void deleteEdges(int key)
        {
            foreach (var list in this.edgesList)
            {
                Edge index = new Edge();
                bool isFound = false;
                for (int i = 0; i < list.Value.Count; i++)
                {
                    if (list.Value[i].v_end == key)
                    {
                        index = list.Value[i];
                        isFound = true;
                    }
                }
                if (isFound)
                    list.Value.Remove(index);
            }
            edgesList.Remove(key);
        }

        //public string peer(int key) //перебираем вершины и возвращаем их метки, если нашли в структуре
        //{
        //    string res = "";
        //    foreach (var list in this.edgesList)
        //    {
        //        string index = "";
        //        bool isFound = false;
        //        for (int i = 0; i < list.Value.Count; i++)
        //        {
        //            if (list.Value[i].v_end == key)
        //            {
        //                index = vertexLabels[list.Key];
        //                isFound = true;
        //            }
        //        }
        //        if (isFound)
        //            res += index.ToString() + " ";
        //    }
        //    return res;
        //}


        public void deleteEdge(string ver1, string ver2)
        {
            int key = -1, key1 = -1;
            key = getKeyByValue(ver1);
            key1 = getKeyByValue(ver2);
            if (key == -1 || key1 == -1)
                return;

            if (isOriented)
            {
                deleteEdge(key, key1);
            }
            else
            {
                deleteEdge(key, key1);
                deleteEdge(key1, key);
            }
            update();
        }

        private void deleteEdge(int key, int key1)
        {
            int index = getEndVertex(key, key1);
            edgesList[key].RemoveAt(index);
        }

        private int getEndVertex(int key, int key1)
        {
            var list = edgesList[key];

            int index = -1;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].v_end == key1)
                    index = i;
            }

            return index;
        }

        /// <summary>
        /// Дерево - связный граф с n-1 ребром.
        /// </summary>
        /// <returns></returns>
        public bool checkTree()
        {
            if (nConnectivityComponents > 1)
                isTree = false;
            return isTree;
        }

        private void initializeArray(Dictionary <int, int> color)
        {
            //for (int i = 0; i < vertexLabels.Count; i++)
            //    color.Add(vertexLabels[i])

            foreach (KeyValuePair<int, string> entry in vertexLabels)
                color.Add(entry.Key, 0);
        }

        /// <summary>
        /// Инициализируем матрицу так чтобы дальше делать из нее матрицу смежностей. 
        /// Если ребра нет, то будет бесконечность на пересечении
        /// </summary>
        /// <param name="n"></param>
        /// <param name="distanceMatr"></param>
        private static void initializeMatrix(int n, int[,] distanceMatr)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                    distanceMatr[i, j] = 1000;
            }
        }

        /// <summary>
        /// Алгоритм Флойда применяем для составления матрицы расстояний. 
        /// На вход подается матрица смежностей (веса на пересечении).
        /// </summary>
        /// <param name="n"></param>
        /// <param name="distanceMatr"></param>
        private void Floyd(int n, int[,] distanceMatr)
        {
            for (int k = 0; k < n; k++)
                for (int i = 0; i < n; i++)
                    for (int j = 0; j < n; j++)
                        distanceMatr[i, j] = Math.Min(distanceMatr[i, j], distanceMatr[i, k] + distanceMatr[k, j]);
        }
        /// <summary>
        /// Алгоритм Хакими
        /// Абсолютный центр — это любая точка на дуге, расстояние от которой до наиболее отдаленной 
        /// вершины графа минимально.
        /// </summary>
        private void findAbsoluteCenter(Dictionary <int,int> dict)
        {
            if (isOriented)
            {
                //Dictionary<int, int> myIndexMap = new Dictionary<int, int>();
                //int nCount = 0;
                //foreach (KeyValuePair<int, string> entry in vertexLabels)
                //{
                //    myIndexMap.Add(entry.Key, nCount);
                //    nCount++;
                //}
                int[,] distanceUndirectedMatrix = getDistanceArray(undirectedEdgesList, dict);
                findAbsCenter(dict, undirectedEdgesList, distanceUndirectedMatrix);
            }
            else
                findAbsCenter(dict, edgesList, distanceMatr);
        }

        private void findAbsCenter(Dictionary<int, int> dict, Dictionary<int, List<Edge>> edList, int[,] distanceArray)
        {
            absoluteCenter.center = "";
            absoluteCenter.result = Double.MaxValue;

            foreach (var vertexList in edList)//беру список вершин у каждой вершины
            {
                int index = dict[vertexList.Key];
                for (int i = 0; i < vertexList.Value.Count; i++)//прохожу по этому списку
                {
                    int index2 = dict[vertexList.Value[i].v_end];
                    for (double t = 0; t <= 1; t += 0.01)//ставлю точку на ребре
                    {
                        double max = 0;
                        foreach (KeyValuePair<int, string> entry in vertexLabels)
                        {
                            int index3 = dict[entry.Key];
                            double d1 = t * vertexList.Value[i].weight + distanceArray[index, index3];
                            double d2 = (1 - t) * vertexList.Value[i].weight + distanceArray[index2, index3];
                            double d = Math.Min(d1, d2);
                            max = Math.Max(d, max);
                        }

                        if (max < absoluteCenter.result)
                        {
                            absoluteCenter.result = max;
                            absoluteCenter.point = t;
                            switch (t)
                            {
                                case 0.0:
                                    absoluteCenter.center = vertexLabels[vertexList.Key];
                                    break;
                                case 1.0:
                                    absoluteCenter.center = vertexLabels[vertexList.Value[i].v_end];
                                    break;
                                default:
                                    absoluteCenter.center = vertexLabels[vertexList.Key] + " " + vertexLabels[vertexList.Value[i].v_end];
                                    break;
                            }
                        }
                    }
                }
            }
        }

        static int lca(int[] match, int[] baseArr, int[] p, int a, int b) //a b - вершины ребра, при рассмт кот найден цикл, находим их предка -b
        {
            bool[] used = new bool[match.Count()];
            while (true)
            {// поднимаемся от вершины a до корня, помечая все чётные вершины
                a = baseArr[a];
                used[a] = true;
                if (match[a] == -1)
                    break;
                a = p[match[a]];
            }
            while (true)
            {// поднимаемся от вершины b, пока не найдём помеченную вершину
                b = baseArr[b];
                if (used[b])
                    return b;
                b = p[match[b]];
            }
        }

        static void markPath(int[] match, int[] baseArr, bool[] blossom, int[] p,
            int v, int b, int children)
        {
           // проходим по пути от вершины до базы цветка, проставляет в blossom true и проставляем предков для чётных вершин
            for (; baseArr[v] != b; v = p[match[v]])
            {
                blossom[baseArr[v]] = blossom[baseArr[match[v]]] = true;
                p[v] = children;
                children = match[v];
            }
        }

        static int findPath(List<List<int>> graph, int[] match, int[] p, int root)
        {
            int n = graph.Count;
            bool[] used = new bool[n];

            for (int i = 0; i < n; i++)
                p[i] = -1;

            int[] baseArr = new int[n];
            for (int i = 0; i < n; ++i)
                baseArr[i] = i;

            used[root] = true;
            int qh = 0; //голова очереди
            int qt = 0; //хвост очереди
            int[] q = new int[n];
            q[qt++] = root; //это очередь - push в очередь
            while (qh < qt)
            {
                int v = q[qh++]; //pop из очереди
                foreach (int to in graph[v])
              //  for (int to =  )
                {
                    if (baseArr[v] == baseArr[to] || match[v] == to)//принадлежат одной сжатой псевдовершине или уже в паросочетании
                        continue;
                    if (to == root || match[to] != -1 && p[match[to]] != -1) //найден цветок...у паросочетания текущей вершины есть предок ( она четная). предки есть только у четных
                    {
                        int curbase = lca(match, baseArr, p, v, to);//ищем базу цветка
                        bool[] blossom = new bool[n];
                        markPath(match, baseArr, blossom, p, v, curbase, to);
                        markPath(match, baseArr, blossom, p, to, curbase, v);
                        for (int i = 0; i < n; ++i)//ищем этот цветок в графе
                            if (blossom[baseArr[i]])
                            {
                                baseArr[i] = curbase;
                                if (!used[i])
                                {
                                    used[i] = true;
                                    q[qt++] = i;
                                }
                            }
                    }
                    else if (p[to] == -1)//иначе обычная вершина
                    {
                        p[to] = v;
                        if (match[to] == -1)
                            return to;//получили удлиняющую цепь
                        var value = match[to];
                        used[value] = true;
                        q[qt++] = value;
                    }
                }
            }
            return -1;
        }

        int[] FindMaxMatch(List<List<int>> graph)
        {
            int n = graph.Count;
            int[] match = new int[n];
            for (int i = 0; i < n; i++)
                match[i] = -1;
           // Arrays.fill(match, -1);
            int[] p = new int[n];
            for (int i = 0; i < n; ++i)
            {
                if (match[i] == -1) //ищем удлиняющую цепь только если вершина i не в паросочетании
                {
                    int v = findPath(graph, match, p, i);   //ищем увеличивающий путь из свободной вершины и возвращаем последнюю вершину этого пути, 
                                                            //либо -1, если увеличивающий путь не найден

                    while (v != -1)                         //чередование
                    {
                        int pv = p[v];
                        int ppv = match[pv];
                        match[v] = pv;
                        match[pv] = v;
                        v = ppv;
                    }
                }
            }
            return match;
        }

        public string FindMaxMatch_EdmondsAlg()
        {
            List<List<int>> adjList = new List<List<int>>();
            for (int i = 0; i < edgesList.Count; i++)
            {
                adjList.Add(new List<int>());
            }
            Dictionary<int, int> myIndexMap = new Dictionary<int, int>();
            int nCount = 0;
            foreach (KeyValuePair<int, string> entry in vertexLabels)
            {
                myIndexMap.Add(entry.Key, nCount);
                nCount++;
            }
            foreach (var vertexList in edgesList)
            {
                int index = myIndexMap[vertexList.Key];
                List<int> list = new List<int>();
                for (int i = 0; i < vertexList.Value.Count; i++)
                {
                    int index2 = myIndexMap[vertexList.Value[i].v_end];
                    adjList[index].Add(index2);
                }
            }
            int[] match = new int[adjList.Count];
            match = FindMaxMatch(adjList);
            int nMatches = 0;
            string res = "";
            for (int i = 0; i < adjList.Count; i++)
            {
                if (match[i] != -1)
                    nMatches++;

                if (i <= match[i])
                    res += vertexLabels[myIndexMap[i]].ToString() + " - " + vertexLabels[match[myIndexMap[i]]].ToString() + "\n";
            }
            res += "Наибольшее паросочетание: " + (nMatches / 2).ToString() + "\n";
            return res;
        }

        public void fillGraphStructure(Microsoft.Win32.OpenFileDialog openFileDialog, FileType fileType)
        {
            clearAllStructures();

            switch (fileType)
            {
                case FileType.adjacencyList:
                    readGraphFromFile_AdjacencyList(openFileDialog);
                    break;
                case FileType.adjacencyMatrix:
                    readGraphFromFile_AdjacencyMatrix(openFileDialog);
                    break;
                case FileType.BergDescription:
                    readGraphFromFile_BergDescription(openFileDialog);
                    break;
                case FileType.edgeList:
                    readGraphFromFile_edgeList(openFileDialog);
                    break;
                case FileType.incidenceMatrix:
                    readGraphIncidenceMatrix_edgeList(openFileDialog);
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }
            update();

        }

        private void update()
        {
            toUndirected();
            createAdjacencyMatrix(edgesList);
        }

        private void clearAllStructures()
        {
            edgesList.Clear();
            if (undirectedEdgesList != null)
                undirectedEdgesList.Clear();
            if (color != null)
                color.Clear();
            vertexLabels.Clear();
            if (maxweight != null)
                maxweight.Clear();
            if (distanceMatr != null)
                Array.Clear(distanceMatr, 0, distanceMatr.Length);

            nConnectivityComponents = -1;
            isOriented = false;
            isTree = true;
            startVerCount = -1; //начальное количество вершин (надо запомнить для добавления новых вершин)
            obhod = "";

            absoluteCenter.center = "";
            absoluteCenter.point = -1.0;
            absoluteCenter.result = Double.MaxValue;
    }

        private void readGraphFromFile_AdjacencyList(Microsoft.Win32.OpenFileDialog openFileDialog)
        {
            using (StreamReader reader = new StreamReader(openFileDialog.FileName))
            {
                string[] readText = File.ReadAllLines(openFileDialog.FileName);
                if (readText[0] == "oriented")
                    isOriented = true;

                //int nCount = -1;
                //foreach (string str in readText)
                for (int nCount = 1; nCount < readText.Count(); nCount++)
                {
                    //nCount++;
                    String[] substrings = readText[nCount].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                    string pattern = @"\b\s+\b";
                    Regex regex = new Regex(pattern);
                    string[] vertexMatches = Regex.Split(substrings[0].Trim(), pattern,
                                    RegexOptions.IgnoreCase);

                    string[] weightMatches = new string[20];
                    if (substrings.Count() > 1)
                    {
                        weightMatches = Regex.Split(substrings[1].Trim(), pattern, RegexOptions.IgnoreCase);
                    }
                    else
                    {
                        for (int i = 0; i < vertexMatches.Length; i++)
                            weightMatches[i] = "1";
                    }

                    int curIndex = -1;
                    if (vertexLabels.ContainsValue(vertexMatches[0]) == false)
                    {
                        curIndex = this.vertexLabels.Count;
                        vertexLabels.Add(curIndex, vertexMatches[0]);
                    }
                    else
                    {
                        curIndex = vertexLabels.FirstOrDefault(x => x.Value == vertexMatches[0]).Key;
                    }

                    List<Edge> list = new List<Edge>();

                    for (int i = 1; i < vertexMatches.Length; i++)
                    {
                        Edge edge = new Edge();

                        if (vertexLabels.ContainsValue(vertexMatches[i]) == false)
                        {
                            int index = vertexLabels.Count;
                            vertexLabels.Add(index, vertexMatches[i]);
                            edge.v_end = index;
                        }
                        else
                            edge.v_end = vertexLabels.FirstOrDefault(x => x.Value == vertexMatches[i]).Key;

                        edge.weight = Convert.ToInt32(weightMatches[i - 1]);

                        list.Add(edge);
                    }
                    this.edgesList.Add(curIndex, list);
                }
            }
            startVerCount = vertexLabels.Count;
        }

        /// <summary>
        /// В файле граф описан в виде матрицы смежностей. 
        /// </summary>
        /// <param name="openFileDialog"></param>
        public void readGraphFromFile_AdjacencyMatrix(Microsoft.Win32.OpenFileDialog openFileDialog)
        {
            string line;
            StreamReader file = new StreamReader(openFileDialog.FileName);

            if (file.ReadLine() == "oriented")
                isOriented = true;

            string[] vertexMatches = file.ReadLine().Split(' ');
            for (int i = 0; i < vertexMatches.Count(); i++)
                vertexLabels.Add(i, vertexMatches[i]);

            int nCount = 0;

            while ((line = file.ReadLine()) != null)
            {
                string[] array = line.Split(' ');
                List<Edge> list = new List<Edge>();
                for (int i = 0; i < array.Count(); i++)
                {
                    Edge ed = new Edge();
                    if (Convert.ToInt32(array[i]) != 0)
                    {
                        ed.weight = Convert.ToInt32(array[i]);
                        ed.v_end = i;
                        list.Add(ed);
                    }
                }
                edgesList.Add(nCount, list);
                nCount++;
            }
            startVerCount = vertexLabels.Count;

        }


        /// <summary>
        /// В файле граф описан в формате описания Бержа. 
        /// </summary>
        /// <param name="openFileDialog"></param>
        public void readGraphFromFile_BergDescription(Microsoft.Win32.OpenFileDialog openFileDialog)
        {
            string line;
            StreamReader file = new StreamReader(openFileDialog.FileName);

            if (file.ReadLine() == "oriented")
                isOriented = true;

            string[] vertexMatches = file.ReadLine().Split(' ');
            for (int i = 0; i < vertexMatches.Count(); i++)
                vertexLabels.Add(i + 1, vertexMatches[i]);

            int nCount = 1;

            while ((line = file.ReadLine()) != null)
            {
                string pattern = @"\b\s+\b";
                Regex regex = new Regex(pattern);
                string[] array = Regex.Split(line.Trim(), pattern, RegexOptions.IgnoreCase);

                int n = Convert.ToInt32(array[0]) + 1;
                List<Edge> list = new List<Edge>();
                for (int i = 1; i < n; i++)
                {
                    Edge ed = new Edge();
                    string[] e = array[i].Split('-');

                    ed.weight = Convert.ToInt32(e[1]);
                    ed.v_end = Convert.ToInt32(e[0]);
                    list.Add(ed);
                }
                edgesList.Add(nCount, list);
                nCount++;
            }
            startVerCount = vertexLabels.Count;

        }


        /// <summary>
        /// В файле граф описан в формате списка ребер.
        /// </summary>
        /// <param name="openFileDialog"></param>
        public void readGraphFromFile_edgeList(Microsoft.Win32.OpenFileDialog openFileDialog)
        {
            string line;
            StreamReader file = new StreamReader(openFileDialog.FileName);

            if (file.ReadLine() == "oriented")
                isOriented = true;

            string[] vertexMatches = file.ReadLine().Split(' ');
            for (int i = 0; i < vertexMatches.Count(); i++)
            {
                vertexLabels.Add(i, vertexMatches[i]);
                List<Edge> list = new List<Edge>();
                edgesList.Add(i, list);
            }

            string[] arrayFrom = null, arrayTo = null, arrayWeight = null;
            string pattern = @"\b\s+\b";
            Regex regex = new Regex(pattern);

            arrayFrom = Regex.Split(file.ReadLine().Trim(), pattern, RegexOptions.IgnoreCase);
            arrayTo = Regex.Split(file.ReadLine().Trim(), pattern, RegexOptions.IgnoreCase);
            arrayWeight = Regex.Split(file.ReadLine().Trim(), pattern, RegexOptions.IgnoreCase);

            startVerCount = vertexLabels.Count;

            for (int i = 0; i < arrayFrom.Count(); i++)
            {
                if (edgesList.ContainsKey(Convert.ToInt32(arrayFrom[i])))
                {
                    Edge ed = new Edge();
                    ed.weight = Convert.ToInt32(arrayWeight[i]);
                    ed.v_end = Convert.ToInt32(arrayTo[i]);
                    edgesList[Convert.ToInt32(arrayFrom[i])].Add(ed);
                }
            }
        }
        /// <summary>
        /// в виде матрицы инцидентности
        /// </summary>
        /// <param name="openFileDialog"></param>
        public void readGraphIncidenceMatrix_edgeList(Microsoft.Win32.OpenFileDialog openFileDialog)
        {
            string line;
            StreamReader file = new StreamReader(openFileDialog.FileName);

            if (file.ReadLine() == "oriented")
                isOriented = true;

            string[] vertexMatches = file.ReadLine().Split(' ');
            for (int i = 0; i < vertexMatches.Count(); i++)
            {
                vertexLabels.Add(i, vertexMatches[i]);
                List<Edge> list = new List<Edge>();
                edgesList.Add(i, list);
            }

            startVerCount = vertexLabels.Count;

            int edCount = Convert.ToInt32(file.ReadLine());
            int[,] matrix = new int[startVerCount, edCount];

            for (int i = 0; i < vertexLabels.Count(); i++)
            {
                string[] arr = file.ReadLine().Split(' '); //считала строку матрицы
                for (int j = 0; j < arr.Count(); j++)
                    matrix[i, j] = Convert.ToInt32(arr[j]);
            }

            string[] weights = file.ReadLine().Split(' ');
            
            for (int j = 0; j < edCount; j++)
            {
                int otkuda = -1;
                int kuda = -1;
                bool ok = false;

                int weight = Convert.ToInt32(weights[j]);
                for (int i = 0; i < startVerCount; i++)
                {
                    if (!ok)
                    {
                        Console.WriteLine(matrix[i, j]);

                        if (matrix[i, j] == 1)
                            otkuda = i;
                        if (matrix[i, j] == -1)
                            kuda = i;

                        if (otkuda != -1 && kuda != -1)
                        {
                            Edge ed = new Edge();
                            ed.weight = weight;
                            ed.v_end = kuda;
                            edgesList[otkuda].Add(ed);
                            ok = true;
                        }
                    }
                }
            }


        }
    }
}

class MyEdgeComparer : IEqualityComparer<Edge>
{
    public bool Equals(Edge x, Edge y)
    {
        return (x.v_end == y.v_end);
    }

    public int GetHashCode(Edge obj)
    {
        throw new NotImplementedException();
    }
}



